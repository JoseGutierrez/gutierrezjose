
package com.mycompany.universidadfrontera;

/**
 *
 * @author Darkitachizero @author Jose miguel Gutierrez Sepulveda
 *  @version 24/05/2020
 * */
public class Ramos {
    private int IdRamos;
    private String Nombre;
    private String Profesor;
/**Constuctor 
     * 
     */
    public Ramos() {
    }
/** obtengo el dato ramos
 * 
 * @return IdRamos
 */

    public int getIdRamos() {
        return IdRamos;
    }
    
    /**Recibo el parametro Idramos
     * 
     * @param IdRamos 
     */
    public void setIdRamos(int IdRamos) {
        this.IdRamos = IdRamos;
    }
    /**funcion para obtener un nombre
     * 
     * @return 
     */

    public String getNombre() {
        return Nombre;
    }
    /**funcion para dar un nombre
     * 
     * @param Nombre 
     */
    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }
    /**aca puedo obtener la variable profesor
     * 
     * @return Profesor
     */
    public String getProfesor() {
        return Profesor;
    }
    /** aca puedo settear o dar el valor a Profesor
     * 
     * @param 
     */
    public void setProfesor(String Profesor) {
        this.Profesor = Profesor;
    }
    
}
