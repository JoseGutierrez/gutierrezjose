
package com.mycompany.universidadfrontera;

/**esta clase se encargara del sistema de asistencia 
 *
 *  @author Jose miguel Gutierrez Sepulveda
 *  @version 24/05/2020
 */
public class SistemaPuntos {
    private int asistencia;
    /** puedo obtener la variable de asistencia
     *  
     * @return  asistencia
     */
    public int getAsistencia() {
        return asistencia;
    }
    /**puedo dar la variable de asistencia 
     * 
     * @param
     */
    public void setAsistencia(int asistencia) {
        this.asistencia = asistencia;
    }
/**Constuctor 
     * 
     */
    public SistemaPuntos() {
    }
}
