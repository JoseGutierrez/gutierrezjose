/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.universidadfrontera;

/** 
 *
 * @author Jose miguel Gutierrez Sepulveda
 */
public class Carrera {
    private int IdCarrera;
    private String NombreCarrera;
/**Constuctor 
     * 
     */
    
    public Carrera() {
    }
    /**puedo obtener la id de carrera
     * 
     * @return 
     */
    public int getIdCarrera() {
        return IdCarrera;
    }
    /** puedo dar una id a una carrera
     * 
     * @param IdCarrera 
     */
    public void setIdCarrera(int IdCarrera) {
        this.IdCarrera = IdCarrera;
    }
    /**puedo obtener el nombre de una carrera
     * 
     * @return 
     */
    public String getNombreCarrera() {
        return NombreCarrera;
    }
    /**puedo dar un nombre de una carrera
     * 
     * @param
     */
    public void setNombreCarrera(String NombreCarrera) {
        this.NombreCarrera = NombreCarrera;
    }
    
    
}
