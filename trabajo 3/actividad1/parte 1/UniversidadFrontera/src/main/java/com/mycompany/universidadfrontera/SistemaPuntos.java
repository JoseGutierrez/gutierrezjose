
package com.mycompany.universidadfrontera;

/**
 *
 * @author Darkitachizero
 */
public class SistemaPuntos {
    private int asistencia;

    public int getAsistencia() {
        return asistencia;
    }

    public void setAsistencia(int asistencia) {
        this.asistencia = asistencia;
    }

    public SistemaPuntos() {
    }
}
