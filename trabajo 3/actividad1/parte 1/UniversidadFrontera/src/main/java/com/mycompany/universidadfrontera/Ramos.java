
package com.mycompany.universidadfrontera;

/**
 *
 * @author Darkitachizero
 */
public class Ramos {
    private int IdRamos;
    private String Nombre;
    private String Profesor;

    public Ramos() {
    }

    public int getIdRamos() {
        return IdRamos;
    }

    public void setIdRamos(int IdRamos) {
        this.IdRamos = IdRamos;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }

    public String getProfesor() {
        return Profesor;
    }

    public void setProfesor(String Profesor) {
        this.Profesor = Profesor;
    }
    
}
