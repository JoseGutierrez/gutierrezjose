
package com.mycompany.universidadfrontera;

/**
 *
 * @author Darkitachizero
 */
public class Alumnos {
    private String Alumno;
    private int Matricula;

    public Alumnos() {
    }

    public String getAlumno() {
        return Alumno;
    }

    public void setAlumno(String Alumno) {
        this.Alumno = Alumno;
    }

    public int getMatricula() {
        return Matricula;
    }

    public void setMatricula(int Matricula) {
        this.Matricula = Matricula;
    }
    
}
